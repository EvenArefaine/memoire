
var buttonColors = ["red", "blue", "green", "yellow"];

var gamePattern = [];

var userClickedPattern = [];

var started = false;

var level = 0;


// Detects the click to start the game

$("#level-title").text("Click ⬜ to Start")

$(".main").hide()

$(".bruh").click(function () {
   $(".main").show(function () {
      if (!started) {

         $("#level-title").text("Level " + level);
         nextSequence();
         started = true;
         $(".bruh").hide();
      }

   });

}

);


// Detects the key to start the game
// $(".bruh").hide();
// $(document).keydown(function () {
//    if (!started) {

//       $("#level-title").text("Level " + level);
//       nextSequence();
//       started = true;
//    }
// }
// );




// Anonymous function that runs when it detects a button click. 
$(".btn").click(function () {

   var userChosenColor = $(this).attr("id");
   userClickedPattern.push(userChosenColor);

   playSound(userChosenColor);

   animatePress(userChosenColor);

   checkAnswer(userClickedPattern.length - 1);

   // console.log(userClickedPattern);
});

// function that checks whether or not the userchosencolor matches to the color in the gamePattern array. 
function checkAnswer(currentLevel) {
   if (userClickedPattern[currentLevel] == gamePattern[currentLevel]) {
      // console.log("success");
      if (userClickedPattern.length == gamePattern.length) {

         setTimeout(function () {
            nextSequence()
         }, 1000);

      }
   }
   else {
      // console.log("wrong");
      playSound("wrong")

      $("body").addClass("game-over");

      setTimeout(function () {
         $("body").removeClass("game-over");
      }, 200);

      $("#level-title").text("Game Over, Click ⬜ to Start Restart");

      startOver();
   }

}

// function for starting over when a player misses the color pattern 
function startOver() {
   level = 0;
   gamePattern = [];
   started = false;
   $(".main").hide();
   $(".bruh").show();

}


// function responsible for generating random color pattern
function nextSequence() {

   userClickedPattern = [];

   level++;

   $("#level-title").text("Level " + level);

   var randomNumber = Math.floor(Math.random() * 4);

   var randomChosenColor = buttonColors[randomNumber];

   gamePattern.push(randomChosenColor);

   $("#" + randomChosenColor).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100)

   playSound(randomChosenColor);
}

// function that plays a sound associated with each button 
function playSound(name) {
   var audio = new Audio("sounds/" + name + ".mp3");
   audio.play();
}

// function responsible for the animation in the game
function animatePress(currentColor) {
   $("#" + currentColor).addClass("pressed");

   setTimeout(function () {
      $("#" + currentColor).removeClass("pressed");
   }, 100);

}

